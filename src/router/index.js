import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/components/layout/Main'
import Home from '@/components/home/Main'
// import Services from '@/components/services/Main'
// import ServicesAditional from '@/components/services-aditional/Main'
// import Products from '@/components/products/Main'
// import Affiliates from '@/components/affiliates/Main'
// import Contact from '@/components/contact/Main'

Vue.use(Router)

export default new Router({
  routes: [{
    path: '/',
    name: 'Layout',
    component: Layout,
    children: [{
        path: '',
        name: 'Home',
        component: Home
      },
      {
        path: 'servicios',
        name: 'Services',
        component: Home
      },
      {
        path: 'servicios-adicionales',
        name: 'ServicesAditional',
        component: Home
      },
      {
        path: 'productos',
        name: 'Products',
        component: Home
      },
      {
        path: 'cementerios-afiliados',
        name: 'Affiliates',
        component: Home
      },
      {
        path: 'contactanos',
        name: 'Contact',
        component: Home
      },
    ]
  }],
  scrollBehaviour(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  }
})

