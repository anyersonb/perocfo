import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  menu: false,
  sectionActive: 1,
}

const getters = {
  menu: state => state.menu,
  sectionActive: state => state.sectionActive

}

const mutations = {
  showMenu(state) {
    state.menu = !state.menu
  },
  changeSectionActive(state) {
    // this.state.sectionActive = section


  }
}

const actions = {
  showMenu: ({
    commit
  }) => commit('showMenu'),
  changeSectionActive: ({
    commit
  }) => commit('changeSectionActive'),
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})


// ///
// import Vue from 'vue'
// import Vuex from 'vuex'

// Vue.use(Vuex)

// // root state object.
// // each Vuex instance is just a single state tree.
// const state = {
//   count: 0
// }

// // mutations are operations that actually mutates the state.
// // each mutation handler gets the entire state tree as the
// // first argument, followed by additional payload arguments.
// // mutations must be synchronous and can be recorded by plugins
// // for debugging purposes.
// const mutations = {
//   increment (state) {
//     state.count++
//   },
//   decrement (state) {
//     state.count--
//   }
// }

// // actions are functions that cause side effects and can involve
// // asynchronous operations.
// const actions = {
//   increment: ({ commit }) => commit('increment'),
//   decrement: ({ commit }) => commit('decrement'),
//   incrementIfOdd ({ commit, state }) {
//     if ((state.count + 1) % 2 === 0) {
//       commit('increment')
//     }
//   },
//   incrementAsync ({ commit }) {
//     return new Promise((resolve, reject) => {
//       setTimeout(() => {
//         commit('increment')
//         resolve()
//       }, 1000)
//     })
//   }
// }

// // getters are functions
// const getters = {
//   evenOrOdd: state => state.count % 2 === 0 ? 'even' : 'odd'
// }

// // A Vuex instance is created by combining the state, mutations, actions,
// // and getters.
// export default new Vuex.Store({
//   state,
//   getters,
//   actions,
//   mutations
// })
