// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import fontawesome from '@fortawesome/fontawesome'
import FontAwesomeIcon from '@fortawesome/vue-fontawesome';
import freeregular from '@fortawesome/fontawesome-free-regular';
import freebrands from '@fortawesome/fontawesome-free-brands';
import freesolid from '@fortawesome/fontawesome-free-solid';
import Modal from "@melmacaluso/vue-modal";

import VueAgile from 'vue-agile';
import AOS from 'aos';
import 'aos/dist/aos.css';
import store from './store/store';
import * as VueGoogleMaps from 'vue2-google-maps';
var VueScrollTo = require('vue-scrollto');
import ScrollView from 'vue-scrollview';
import VueYouTubeEmbed from 'vue-youtube-embed';
import VModal from 'vue-js-modal';


Vue.use(VModal)
Vue.use(VueYouTubeEmbed)
Vue.use(ScrollView)
Vue.use(VueScrollTo)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAIjofh1OzK71knf-WQrGzvLTZCbyDavo8',
    // key: 'AIzaSyDjrS1Wisa8OoF4xxyyjCRmurxXueZ7TNI',
    libraries: 'places',
  },
})

Vue.use(VueAgile);
fontawesome.library.add(freeregular, freebrands, freesolid);
Vue.component(FontAwesomeIcon.name, FontAwesomeIcon);
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',

  store,
  router,
  components: {
    App
  },
  template: '<App/>',
  created() {
    AOS.init()
  },
})
